﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.Connectivity;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Converters;
using System.Net.Http;
using Json.Net;
using System.Globalization;

namespace CS481_HW6
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();    
        }

        /*Checks for internet status and returns a boolean of whether its
         * connected to the internet or not
         */
        public bool DoIHaveInternet()
        {
            return CrossConnectivity.Current.IsConnected;
        }

        async void GetDefinitionClicked(object sender, System.EventArgs e)
        {

            //check if there's internet when doing a search
            if(DoIHaveInternet())
            {
                // create HTTP client and add authorization token thats stored in separate class
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "Token " + ApiKeys.DictionaryKey);

                //create uri and pass in the url path concantenated with the word that is in the search field on the page
                var uri = new Uri(
                string.Format($"https://owlbot.info/api/v4/dictionary/" + $"{searchBar.Text}"));

                // create the request message, which will be a GET message since this is asking to get data based on search value
                var request = new HttpRequestMessage();
                request.Method = HttpMethod.Get;
                request.RequestUri = uri;
                //create response message and dictionaryData object variable
                HttpResponseMessage response = await client.SendAsync(request);
                DictionaryItem dictData = null;
                try
                {
                    // if the response says it was successful
                    if (response.IsSuccessStatusCode)
                    {
                        //parse the JSON data into a dictionaryItem object and bind it to the page labels
                        var content = await response.Content.ReadAsStringAsync();
                        dictData = DictionaryItem.FromJson(content);
                        BindingContext = dictData;
                    }
                    else
                    {
                        //if the request was unsuccessful, print to the console why it failed
                        Console.WriteLine("response was not success status code");
                        Console.WriteLine("response code is: " + response.StatusCode.ToString());
                        await DisplayAlert("Search Failed", "error:" + response.StatusCode.ToString(), "OK").ConfigureAwait(false);
                    }
                }
                catch (Exception d)
                {
                    Console.WriteLine(d.ToString());
                }
            }
            else
            {
                //if device is not connected to internet, raise a flag
                await DisplayAlert("Wait!", "You need to connect to the internet to do a search", "OK").ConfigureAwait(false);
            }

        }


        /*Inner class just to store API keys so there's only one place to update it.
         * In this scenario, this is not necessary and the string of the key could have just
         * been pasted in the client.DefaultRequestHeaders.Add() method
         */
       private class ApiKeys
        {
          public static string DictionaryKey = "b3c298fb623cf394478198f093b6e6050f72df72";
        }
    }
    public partial class DictionaryItem
    {
        [JsonProperty("definitions")]
        public Definitions[] Defntn { get; set; }


        [JsonProperty("word")]
        public string word { get; set; }


        [JsonProperty("pronunciation")]
        public string pronunciation { get; set; }

        public static DictionaryItem FromJson(string json) => JsonConvert.DeserializeObject<DictionaryItem>(json, CS481_HW6.Converter.Settings);
    }

    public partial class Definitions 
    {
        [JsonProperty("type")]
        public string type { get; set; }

        [JsonProperty("definition")]
        public string definition { get; set; }

        [JsonProperty("example")]
        public string example { get; set; }

        [JsonProperty("image_url")]
        public string image_url { get; set; }

        [JsonProperty("emoji")]
        public string emoji { get; set; }

        public static Definitions FromJson(string json) => JsonConvert.DeserializeObject<Definitions>(json, CS481_HW6.Converter.Settings);

    }

    public static class Serialize
    {
        public static string ToJson(this DictionaryItem self) => JsonConvert.SerializeObject(self, CS481_HW6.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}


